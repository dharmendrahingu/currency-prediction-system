## Currency Prediction System ##

Paste the important links here!

Python building predictive models:
https://www.analyticsvidhya.com/blog/2015/09/build-predictive-model-10-minutes-python/

Review and description:
http://www.investopedia.com/articles/forex/11/4-ways-to-forecast-exchange-rates.asp

Data:
http://investexcel.net/automatically-download-historical-forex-data-into-excel/

ARIMA Model using Python: 
http://machinelearningmastery.com/arima-for-time-series-forecasting-with-python/
https://www.analyticsvidhya.com/blog/2016/02/time-series-forecasting-codes-python/

Paper for using ANN to predict:
https://pdfs.semanticscholar.org/88d3/dfe825c07dec8362509d0dfc64fed2a43d28.pdf